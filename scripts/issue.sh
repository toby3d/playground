#!/bin/bash

# Fetch last issue
curl --request GET \
  --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" \
  "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/issues?per_page=1&scope=created-by-me&state=opened"
ISSUE_AUTHOR_ID=`echo $ISSUE | jq --raw-output '.[0].author.id'`

# Check author of issue
ISSUE_ID=`echo $ISSUE | jq --raw-output '.[0].iid'`
ISSUE_TITLE=`echo $ISSUE | jq --raw-output '.[0].title'`
ISSUE_DESCRIPTION=`echo $ISSUE | jq --raw-output '.[0].description'`
ISSUE_CREATED_AT=`echo $ISSUE | jq --raw-output '.[0].created_at'`
ISSUE_UPDATED_AT=`echo $ISSUE | jq --raw-output '.[0].updated_at'`

FILE_CONTENT=`curl --request GET --header 'PRIVATE-TOKEN: ${GITLAB_API_TOKEN}' "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/repository/files/data%2Fexample%2Eyaml/raw?ref=master"`

# Write meta into data file
curl --request PUT \
  --header 'PRIVATE-TOKEN: ${GITLAB_API_TOKEN}' \
  --header "Content-Type: text/plain" \
  --data '{"branch": "master", "author_email": "${GITLAB_USER_EMAIL}", "author_name": "${GITLAB_USER_NAME}", "content": "${FILE_CONTENT}\n  - id: ${ISSUE_ID}\n    title: ${ISSUE_TITLE}\n    description: ${ISSUE_DESCRIPTION}\n", "commit_message": "update file"}' \
  "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/repository/files/data%2Fexample%2Eyaml"

# Close the issue
curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/issues/${ISSUE_ID}/notes?body=%F0%9F%A4%96%20Okay%2C%20done%21"
